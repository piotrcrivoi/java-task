﻿/**
 * Заполнить двумерный массив размером 5x5
 * так, как представлено на рис.
 */

import java.util.Scanner;

class TaskCh12N028 {

  public static void main(String[] args) {
    System.out.println("Введите размерность массива: ");
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();

    int[][] array = new int[n][n];

    int row = 0;
    int col = 0;
    int indexCol = 1;
    int indexRow = 0;
    int turn = 0;
    int checkOut = array.length;

    for (int i = 0; i < array.length*array.length ; i++) {
      array[row][col] = i + 1;
      if (--checkOut == 0) {
        checkOut = array.length * (turn % 2) + array.length * ((turn + 1) % 2) - (turn / 2 - 1) - 2;
        int cell = indexCol;
        indexCol = -indexRow;
        indexRow = cell;
        turn++;
      }

      col += indexCol;
      row += indexRow;
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        System.out.printf(" %3d ",array[i][j]);
      }
      System.out.println();
    }
  }
}