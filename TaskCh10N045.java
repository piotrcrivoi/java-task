﻿/**
 * Даны первый член и разность арифметической прогрессии. Написать рекурсивную
 *  функцию для нахождения:
 * а) n-го члена прогрессии;
 * б) суммы n первых членов прогрессии.
 * @author Crivoi Piotr
 */

import java.util.Scanner;

class TaskCh10N045 {

  public static void main(String[] args) {


    Scanner in = new Scanner(System.in);
    Progression p = new Progression();
    System.out.print("Введите первый элемент: ");
    int first = in.nextInt();
    System.out.print("Введите разность прогрессии: ");
    int d = in.nextInt();
    System.out.print("Введите индекс члена: ");
    int n = in.nextInt();


    System.out.println(n + "-ый элемент = " + p.prog(first, d, n));
    System.out.println("Сумма "+ n +" первых членов прогрессии = " + p.sum(first, d, n));

  }
}
class Progression {
  int prog(int first, int d, int n) {
    int result;
    if (n == 1) {
      return result = first;
    } else {
      result = prog(first, d, n - 1) + d;
      return result;
    }

  }

  int sum(int first, int d, int n) {
    int result = 0;
    if (n == 1) {
      return result = first;
    } else {
      for (int i = 1; i < n; i++) {
        result += (prog(first, d, first ++) + d);
      }
      return result;
    }
  }
}


