﻿/**
 * Написать рекурсивную функцию для вычисления максимального элемента
 * массива из n элементов.
 *
 * @author Crivoi Piotr
 */

import java.util.Random;
import java.util.Scanner;

class TaskCh10N048 {

  public static void main(String[] args) {


    Scanner in = new Scanner(System.in);
    Array a = new Array();
    final Random random = new Random();

    System.out.println("Введите размерность массива: ");
    int n = in.nextInt();

    int[] arr = new int[n];

    for (int i = 0; i < arr.length; i++) {
      arr[i] = random.nextInt(100);
    }
    for(int i = 0; i < arr.length; i++) {
      System.out.println(arr[i] + " ");
    }

    System.out.println("Максимальный элемент = " + a.max(arr,1, n));
  }
}

class Array {
  int max(int arr[], int index, int n) {
    int max;

    if (index >= n - 2) {
      if (arr[index] > arr[index + 1]) {
        return arr[index];
      } else {
        return arr[index + 1];
      }
    }

    max = max(arr, index + 1, n);

    if (arr[index] > max)
      return arr[index];
    else
      return max;
  }
}