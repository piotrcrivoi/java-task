/**
 * "�������� ���". ����� ������� ������������ �� ������, �������
 * ��������� �� ���������� 1 �� �� ����. ����� �� ����� ������, �� �����
 * ����������, ��� ����� ������ ����� ���������� ����, � ������������ �����. ������
 * �������, �� ������ �������, ��������, ��� ���������� ��������� �� ������.
 * ������ 1/3�� �� ����������� � ������, �� ����� ��������, ��� �����
 * ��������� ��������, ���� ��� � �� �������� ����. �� ���� ���, ������ ���
 * �������� ������, �� �������� 1/4��. ��� �� ���������� ��������, � �����
 * N-�����, ������ 1/N ��, ����� ������ �������.
 * ����������:
 * �) �� ����� ���������� �� ���� ����� ���������� ������� ����� 100-�� �����
 * (���� ���������, ��� ����� ��������);
 * �) ����� ����� ���� �� ��� ���� �������.
 * @author Crivoi Piotr
 */

import java.util.Scanner;

public class TaskCh05N038 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
      int i;
      float g = 0;
      float s = 0;

    System.out.print("������� ���������� ��������� �������: ");
      int n = in.nextInt();

    for (i = 1; i <= n; i++) {
      if (i % 2 != 0) s = s + 1/(float)i;
        else if (i % 2 == 0) s = s - 1/(float)i;
      g = g + 1/(float)i;
    }
    System.out.println("���������� �� ���� = " + s + "\n����� ���� = " + g);
  }
}