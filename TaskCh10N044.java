/**
 * �������� ����������� ������� ���������� ��������� ����� ������������
 * �����. �������� ������ ������� ����� ���������� ��������� �������. ��-
 * �� ������� ��� ����� ����� �����, ����� ��� ����� ��������� �����
 * � ��������� ���� �������, �� � ���������� ����� �������� ����������� �����,
 * ������� � ���������� �������� ������ ������� �����.
 * @author Crivoi Piotr
 */


import java.util.Scanner;

class TaskCh10N044 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    Calculation objCalculation = new Calculation();

    System.out.print("������� ����� = ");
    int n = in.nextInt();

    System.out.println("�������� ������ = " + objCalculation.sqrtDigital(n));
  }
}



class Calculation {
  int sqrtDigital(int n) {

    int result = 0;
    int digital = 0;

    if (n == 0)
      return 0;
    else
      result = sqrtDigital(n / 10) + n % 10;
    digital = sqrtDigital(result / 10) + result % 10;
    return digital;
  }
}