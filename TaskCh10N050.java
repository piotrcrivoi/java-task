﻿/**
 * Написать рекурсивную функцию для вычисления значения так называемой
 * функции Аккермана для неотрицательных чисел n и m. Функция Аккермана
 * определяется следующим образом.
 */

import java.util.Scanner;

class TaskCh10N050 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    Gilbert acker = new Gilbert();
    System.out.println("Введите n: ");
    int n = in.nextInt();
    System.out.println("Введите m: ");
    int m = in.nextInt();

    System.out.println(acker.ackermann(n,m))

  }
}
class Gilbert {
  int ackermann(int n, int m) {
    if (n == 0) {
      return m+1;
    } else if (n > 0 && m == 0){
      return ackermann(n-1,1);
    } else   {
      return ackermann(n-1,ackermann(n,m-1));
    }
  }
}