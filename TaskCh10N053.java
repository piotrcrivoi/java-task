﻿/**
 * Написать рекурсивную процедуру для ввода с клавиатуры последовательности
 * чисел и вывода ее на экран в обратном порядке.
 * (окончание последовательности — при вводе нуля).
 * @author Crivoi Piotr
 *
 */

import java.util.Scanner;

class TaskCh10N053 {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    Reverse back = new Reverse();
    int k = 0;
    System.out.print("Введите последователность чисел: ");
    String row = in.nextLine();
    System.out.println(row);
    String[] array = row.split(" ");
    int[] numbers = new int[array.length];


    for (int i = 0; i < array.length; i++) {
      numbers[i] = Integer.parseInt(array[i]);
    }

    for (int i = 0; i < array.length; i++) {
      if (numbers[i] != 0) {
        System.out.print(numbers[i] + "    ");
        k++;
      } else {
        break;
      }
    }
    System.out.println();
    System.out.print("Числовой ряд в обратном порядке : ");
    back.flip(numbers,k,0);

  }
}

class Reverse {
  void flip(int[] numbers, int k,int count) {
    if (k == 1) {
      System.out.println(numbers[k-1]);
    } else if (count < k) {

      System.out.print(numbers[k - count - 1] + " ");
      flip(numbers, k, count + 1);
    }
  }
}