/**
 * ��������� ���������, ������� � ����������� �� ����������� ������ ���
 * ������ (1, 2, ..., 12) ������� �� ����� ����� ����, � �������� ���������
 *  ���� �����.
 *  @author Crivoi Piotr
 */

import java.util.Scanner;


class TaskCh04N106 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    String seasonString;

    System.out.print("����� ����� = ");
    int month = in.nextInt();

    switch (month) {
      case 1:
      case 2:
      case 12:
        seasonString = "����";
        System.out.print("������ " + seasonString);
        break;
      case 3:
      case 4:
      case 5:
        seasonString = "�����";
        System.out.print("������ " + seasonString);
        break;
      case 6:
      case 7:
      case 8:
        seasonString = "����";
        System.out.print("������ " + seasonString);
        break;
      case 9:
      case 10:
      case 11:
        seasonString = "�����";
        System.out.print("������ " + seasonString);
        break;
      default: seasonString = "������ ������ ���";
        System.out.print(seasonString);
        break;
    }
  }
}