﻿/**Написать рекурсивную процедуру для вывода на экран цифр натурального
 * числа в обратном порядке.
 *
 * @author Crivoi Piotr
 */

import java.util.Scanner;

class TaskCh10N052 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    Reverce reverce = new Reverce();

    System.out.println("Введите n: ");
    int n = in.nextInt();
    reverce.back(n);


      }
    }
    class  Reverce {
       int back(int n) {
         if (n < 10) {
           System.out.println(n);
         return n;
       }
        else {
        System.out.print(n % 10);
           return back(n / 10);
        }

      }
    }