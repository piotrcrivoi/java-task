﻿/**Определить результат выполнения следующих рекурсивных процедур при n = 5;
 *
 * @author Crivoi Piotr
 */

import java.util.Scanner;

class TaskCh10N051 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    TaskA a = new TaskA();
    TaskB b = new TaskB();
    TaskC c = new TaskC();

    System.out.println("Введите n: ");
    int n = in.nextInt();

    System.out.println("Task A: ");
    a.taskA(n);
    System.out.println();
    System.out.println("Task B: ");
    b.taskB(n);
    System.out.println();
    System.out.println("Task C: ");
    c.taskC(n);
  }
}
class  TaskA {
  void taskA(int n) {
    if (n > 0) {
      System.out.print(n + " ");
      taskA(n - 1);
    }

  }
}
class   TaskB {
  void taskB(int n) {
    if (n > 0) {
      taskB(n - 1);
      System.out.print(n + " ");
    }
  }
}
class  TaskC {
  void taskC(int n) {
    if (n > 0) {
      System.out.print(n + " ");
      taskC(n - 1);
      System.out.print(n + " ");
    }
  }
}