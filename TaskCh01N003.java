﻿/**
 * Составить программу вывода на экран числа, вводимого с клавиатуры. 
 * Выводимому числу должно предшествовать сообщение "Вы ввели число"
 * @author Crivoi Piotr :)
 */


import java.util.Scanner;


  public class TaskCh01N003 {

    public static void main(String[] args) {

      Scanner in = new Scanner(System.in);

        System.out.print("Задай число = ");
          int num = in.nextInt();
        System.out.println("Вы ввели число " + num);
    }
  }