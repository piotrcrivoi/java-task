**
 * ������ �������� �������������� ���������, � ������� ������������
 * ������� ������, � ��� ����� ���������. ���������, ��������� �� � ���
 * ����������� ������.
 * �) ������� ������ ������� ����� �� ��� ���.
 * �) � ������ �������������� ����������� ������:
 * ���� ������� ������ ������ (�����������) ������, �� ������
 * ��������� � ��������� ������� ������ ����� ������;
 * ���� ������� ������ ����� (�����������) ������, �� ������
 * ��������� � ��������� ���������� ����� ������.
 * ���� ������ ����������� ���������, �� �������� �� ����.
 * @author Crivoi Piotr
 */

import java.util.Scanner;

public class TaskCh09N185 {

  public static void  main(String[] args) {

    int countOpenBracket = 0;
    int countCloseBracket = 0;

    Scanner in = new Scanner(System.in);

    System.out.print("������� ���������: ");

    StringBuilder strBuilder = new StringBuilder(in.nextLine());

    for( int i=0;i < strBuilder.length();i++) {
        if (strBuilder.charAt(i) == '(') countOpenBracket++;
        if (strBuilder.charAt(i) == ')') countCloseBracket++;
    }
      if (countOpenBracket == countCloseBracket) {
      System.out.print("��� ������ ���������� �����");
      } else if (countOpenBracket > countCloseBracket){
      System.out.print("���������� ������ ����������� ������ - " + (countOpenBracket - countCloseBracket));
        } else if (countCloseBracket > countOpenBracket) {
            for( int i= strBuilder.length()-1;i >= 0;i--) {
                if (strBuilder.charAt(i) == ')')  {
                  System.out.print("������� ������ ����������� ������ - " + i);
                  break;
                }
            }
          }
  }
}