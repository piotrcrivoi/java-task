﻿/**
 * Написать рекурсивную функцию, определяющую, является ли заданное на-
 * туральное число простым (простым называется натуральное число, большее
 * 1, не имеющее других делителей, кроме единицы и самого себя).
 * оптимизировать проверку числа делителей
 * @author Crivoi Piotr
 */

import java.util.Scanner;

class TaskCh10N56 {

  public static void main(String args[]) {

    Scanner in = new Scanner(System.in);
    simpleNumber p = new simpleNumber();

    System.out.print("Введите N: ");
    int n = in.nextInt();

    int check = p.prime(n,n/2);
    if (check == 1) {
      System.out.println(n + " является простым числом");
    } else System.out.println(n + " не является простым числом");
  }
}
class simpleNumber {
  int prime(int n,int i) {
    if (i == 1) {
      return 1;
    } else if ( n % i == 0) {
      return 0;
    } else {
      return prime(n,i-1);
    }

  }
}