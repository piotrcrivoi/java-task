﻿import  java.util.Scanner;
public class CoolJavaProjects {

    public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
      Graduates graduates = new Graduates();
      SelfLearner selfLearner = new SelfLearner();
      Students politS = new SelfLearner(); // Выделяем обьект класса Student , который входит в класс SelfLearner.
      Students politG = new Graduates(); // Выделяем обьект класса Student , который входит в класс Graduates.
      Employer work = new Employer();

      System.out.println("---------------------------------------------");
      System.out.println("---------Company CoolJavaProject-------------");
      System.out.println("----------------Interview--------------------");
      for ( int i =0; i < 10; i++) {

        System.out.println(work.getWelcome());
          System.out.print("- I'm ");
          String name = in.nextLine();


        if ( i % 2 == 0) {
          graduates.setName(name);
          selfLearner.setName(name);
          graduates.getName();
          System.out.println("- " + graduates.getName() + " " + work.getIntroduce());
          in.nextLine();
          System.out.println("- " + graduates.introduceYourSelf());
          in.nextLine();
          System.out.println(work.getThank() + " " +  graduates.getName() + " " + work.getNiceDay());
          in.nextLine();
          System.out.println(graduates.sayGoodBye());
          in.nextLine();
          System.out.println(work.getGoodBye() + " " + graduates.getName());
          System.out.println(politG.politeness());
          in.nextLine();
        } else {

          selfLearner.setName(name);
          selfLearner.getName();
          System.out.println("- " + selfLearner.getName() + " " + work.getIntroduce());
          System.out.println("- " + selfLearner.introduceYourSelf());
          in.nextLine();
          System.out.println(work.getThank() + " " + selfLearner.getName() + " " + work.getNiceDay());
          in.nextLine();
          System.out.println(selfLearner.sayGoodBye());
          in.nextLine();
          System.out.println(work.getGoodBye() + ", " + selfLearner.getName());
          System.out.println(politS.politeness());
          in.nextLine();
        }
        if (i < 9) {
          System.out.println(work.getNext());
          in.nextLine();
        } else System.out.println("-------------The End Interview-------------------");
      }
    }
}

/*
 * Принцип  ООП , абстракция. Создаем абстрактный класс, в котором котором впишем абстрактные и неабстрактные методы.
 * В этом же классе создаем 2 конструктора: Пустой и с переменной "private", которую мы можем использовать только в этом
 * классе. Используем метод Сеттеров и Геттеров для включения принципа Инкапсуляции.
 */
public abstract class Students {
  private String name;
  

  public Students (String name, String lastName) {
    this.name = name;
    
  }
  public Students() {
  }

  public void setName(String name) {

    this.name = name;
  }
  
  public String getName() {

    return name;
  }

  

  public String politeness() {
    return "When can I find out the answer"; } // Создание метода, который мы перезапишем в классах наследниках
  public abstract String introduceYourSelf();
}
//------------------------
public class Employer {
  public String getWelcome() {
    return "- Hi, Who are you? ";
  }
  public String getIntroduce() {
    return ", introduce yourself and describe your java experience,please.";
  }
  public  String getThank() {
    return "- Thank you for answer, ";
  }
  public  String getNiceDay() {
    return ", have a nice day";
  }
  public String getGoodBye() {
    return "- Goodbye";
  }
  public String getNext() {
    return  "- Next student, please.";
  }
}

/*
 * Класс наследник, наследуюет все абстрактные методы , а также заключает контракт с Интерфейсом "GoodBye"
 * на выполнение всех методов, которые включены в Интерфейс.
 *
 */
public class SelfLearner extends Students implements GoodBye {



  @Override
  public String introduceYourSelf() {
    return "- I have been learning Java by myself, nobody examined how through is my knowledge and how good is my code";
  }
  public String sayGoodBye(){
    return "- Thank you for the opportunity to come.";
  }
  public String politeness() { // перезаписываем метод  для реализации Полиморфизма.
    return "- Goodbye, have a nice day";
  }
}

/*
 * Класс наследник, наследуюет все абстрактные методы , а также заключает контракт с Интерфейсом "GoodBye"
 * на выполнение всех методов, которые включены в Интерфейс.
 *
 */
public class Graduates extends Students implements GoodBye{

  @Override
  public String introduceYourSelf() {
    return "I passed successfully getJavaJob exams and code reviews.";
  }

  public String sayGoodBye(){
    return "- Thank you for the opportunity to come.";
  }
  public String politeness(){ // перезаписываем метод  для реализации Полиморфизма.
    return "- Goodbye, I hope to see you soon";
  }
}

public interface GoodBye { // Интерфейс - контракт на выполнения методов.
  String sayGoodBye();
}


