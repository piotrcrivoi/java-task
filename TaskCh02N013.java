/**
 * ���� ����������� �����. ����� �����, ���������� ��� ��������� ��� ����
 * ������ ������.
 * �������: (200>n>100)
 * @author Crivoi Piotr
 */


import java.util.Scanner;


public class TaskCh02N013 {

  public static void main(String[] args) {
  
	Scanner in = new Scanner(System.in);

    System.out.print("Ǉ������ ����� = ");
    int n = in.nextInt();
	int m = 0;

    if (n > 100 && n < 200) {
      for (int i = 0; i < 3; i++) {
        m *= 10;
        m += n % 10;
        n /= 10;
      }
      System.out.println(m);
    }
    else System.out.print("Ǉ������� ����� - " + n + ", ��������� ��� ����������");
  }
} 