/**
 * ��������� ���������, ������� ����� ���� �����, ��������� ������ ��������
 * ��� ���� � ���������. ���������� �����, ���������� ��������� � ���� ����,
 * ����� ���� ����� 1, 2 ��� 3. ����� ������ ��������� ���� �������� ��
 * �����. ����� ��������� ���� ������ �������� ��������� � ������� �����
 * �������-��������������. ��������� ���� ������� ������������ ������
 * ���������� �����, ������� ����.
 *
 * @author Crivoi Piotr
 */


import java.util.Scanner;

public class TaskCh06N067 {
  public static void main (String[] args) {

    Game gameObj = new Game();

    gameObj.getTeam();
    gameObj.play();
    gameObj.getResult();
  }
}

class Game {
  private int scoreTeamOne;
  private String TeamOne;
  private int scoreTeamTwo;
  private String TeamTwo;

  void getTeam() {

    Scanner in = new Scanner(System.in);
    System.out.println("Enter team one name");
    TeamOne = in.nextLine();
    System.out.println("Enter team two name");
    TeamTwo = in.nextLine();
  }

  void play() {

    Scanner in = new Scanner(System.in);

    int n;
    int score;

    do {
      System.out.println("Enter team to score (1 or 2 or 0 to finish game)");
      n = in.nextInt();
      if (n == 1) {
        System.out.println("Enter score (1 or 2 or 3)");
        score = in.nextInt();
        if (score == 1 || score == 2 || score == 3) {
          scoreTeamOne +=score;
          System.out.println(TeamOne +" " + scoreTeamOne +" : " + scoreTeamTwo + " "+TeamTwo );
        }
        else {
          System.out.println("Wrong value. Re-enter,please.");
        }
      }
      else if (n == 2) {
        System.out.println("Enter score (1 or 2 or 3)");
        score = in.nextInt();
        if (score == 1 || score == 2 || score == 3) {
          scoreTeamTwo += score;
          System.out.println(TeamOne +" " + scoreTeamOne +" : " + scoreTeamTwo + " "+TeamTwo );
        }
        else {
          System.out.println("Wrong value. Re-enter,please.");
        }
      }
    } while (n != 0);
  }

  void getResult() {

    if (scoreTeamOne > scoreTeamTwo)
      System.out.println(TeamOne +" " + scoreTeamOne +" : " + scoreTeamTwo + " "+TeamTwo + "\nWinner - " + TeamOne);
    else if (scoreTeamTwo > scoreTeamOne)
      System.out.println(TeamOne +" " + scoreTeamOne +" : " + scoreTeamTwo + " "+TeamTwo + "\nWinner - " + TeamTwo);
    else
      System.out.println(TeamOne +" " + scoreTeamOne +" : " + scoreTeamTwo + " "+TeamTwo + "\nResult - draw");
  }
