﻿/**
 * Заполнить двумерный массив размером N x N
 * так, как показано на рис.
 *
 * @author Crivoi Piotr
 */
import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    ArrayA  arrA = new ArrayA();
    ArrayB  arrB = new ArrayB();
    ArrayC  arrC = new ArrayC();
    Scanner in = new Scanner(System.in);

    System.out.print("Введите размерность: ");
    int n = in.nextInt();

    int array[][] = new int[n][n];
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < array.length; j++) {
        array[i][j] = 0;
      }
    }
    arrA.main(array);
    arrB.main(array);
    arrC.main(array);

  }
}
class ArrayA {
   void main( int[][] array) {
    System.out.println("Массив А:");
    for(int i = 0; i<array.length; i++) {
      for(int j = 0; j < array.length; j++) {
        if (i == j) array[i][j] = 1;
        else if (i + 1 == array.length - j) array[i][j] = 1;
      }
    }

    for(int i = 0; i<array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        System.out.print(array[i][j] + " ");
      }
      System.out.println();
    }
  }
}
class ArrayB {
   void main( int[][] array) {
    System.out.println("Массив В:");
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        array[i][j] = 0;
      }
    }
    for(int i = 0; i<array.length; i++) {
      for(int j = 0; j < array.length; j++) {
        if(i == j) {
          array[i][j] = 1;
        } else if(i+1 == array.length-j) {
          array[i][j] = 1;
        } else if(j==(array.length-1)/2) {
          array[i][j] = 1;
        } else if(i == (array.length - 1) / 2) {
          array[i][j] = 1;
        }

      }
    }
    for(int i = 0; i<array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        System.out.print(array[i][j] + " ");
      }
      System.out.println();
    }
  }
}
class ArrayC {
   void main( int[][] array) {
    System.out.println("Массив C:");
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        array[i][j] = 0;
      }
    }
    for (int i = 0; i <= array.length / 2; i++) {

      for (int j = i; j < array[i].length - i; j++) {
        array[i][j] = array[array.length - i - 1][j] = 1;
      }
    }
    for (int i = 0; i < array.length ; i++) {
      for (int j = 0; j < array.length ; j++) {
        if ( i == 0 || i == array.length - 1) {
          array[i][j] = 1;
        }
      }
    }
    for(int i = 0; i<array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        System.out.print(array[i][j] + " ");
      }
      System.out.println();
    }
  }
}