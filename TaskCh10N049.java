﻿/**
 * Написать рекурсивную функцию для вычисления индекса максимального
 * элемента массива из n элементов.
 * @author Crivoi Piotr
 */

import java.util.Random;
import java.util.Scanner;

class TaskCh10N049 {

  public static void main(String[] args) {


    Scanner in = new Scanner(System.in);
    Array a = new Array();
    final Random random = new Random();

    System.out.println("Введите размерность массива: ");
    int n = in.nextInt();

    int[] arr = new int[n];

    for (int i = 0; i < arr.length; i++) {
      arr[i] = random.nextInt(100);
    }
    for (int i = 0; i < arr.length; i++) {
      System.out.println(arr[i] + " ");
    }

    System.out.println("Индекс максимального элемента = " + a.findMaxIndex(arr,1));
  }
}

class Array {
  int findMaxIndex(int arr[], int count) {
    if ( count == arr.length - 1) {
      return count;
    }
       int index =  findMaxIndex(arr,count +1);
      if ( arr[count] > arr[index]) {
      return count;
       } else {
      return index;
        }
   }
}
