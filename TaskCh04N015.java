﻿/**
 * Известны год и номер месяца рождения человека, а также год и номер месяца
 * сегодняшнего дня (январь — 1 и т. д.). Определить возраст человека (число
 * полных лет). В случае совпадения указанных номеров месяцев считать, что
 * прошел полный год.
 * test 1 выполнен успешно.
 * test 2 выполнен успешно.
 * test 3 выполнен успешно.
 * @author Crivoi Piotr.
 */


import java.util.Scanner;


class TaskCh04N015 {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
      int out;

    System.out.println("Введите год :");
      int dateYear = in.nextInt();
    System.out.println("Введите месяц :");
      int dateMonth = in.nextInt();

    System.out.println("Введите  сегодняшний год :");
      int nowYear = in.nextInt();
    System.out.println("Введите  сегодняшний месяц: ");
      int nowMonth = in.nextInt();

    out = nowYear - dateYear;
      if (dateMonth > nowMonth) System.out.println("Вам " + (out - 1) + " лет");
        else System.out.println("Вам " + out + " лет");
  }
}
