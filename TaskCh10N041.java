/**
 * �������� ����������� ������� ��� ���������� ���������� ������������
 * ������������ ����� n.
 * @author Crivoi Piotr
 */

import java.util.Scanner;

class TaskCh10N041 {

  public static void main(String args[]) {

    Scanner in = new Scanner(System.in);
    Factorial f = new Factorial();

    System.out.print("������� N: ");
      int n = in.nextInt();
    System.out.print("��������� ����� " + n + ", ����� = " + f.fact(n));
  }
}
class Factorial {
  int fact(int n) {
    int result;
    if (n == 1) return 1;
    else {
      result = fact(n - 1) * n;
      return result;
    }
  }
}