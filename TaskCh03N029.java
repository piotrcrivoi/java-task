/**
 * �������� �������, ������� �������� ��������, �����:
 * �)������ �� ����� X � Y ��������;
 * �)������ ���� �� ����� X � Y ������ 20;
 * �)���� �� ���� �� ����� X � Y ����� ����;
 * �)������ �� ����� X, Y, Z �������������;
 * �)������ ���� �� ����� X, Y � Z ������ ����;
 * �)���� �� ���� �� ����� X, Y, Z ������ 100.
 *
 * @author Crivoi Piotr
 */
 
 
 class TaskCh03N029 {

  public static boolean taskA(int x, int y) {
    return x % 2 != 0 && y % 2 != 0;
  }

  public static boolean taskB(int x, int y) {
    return x <= 2 && y > 2 || x > 2 && y <= 2;
  }

  public static boolean taskV(int x, int y) {
    return x == 0 || y == 0;
  }

  public static boolean taskG(int x, int y, int z) {
    return x < 0 && y < 0 && z < 0;
  }

  public static boolean taskD(int x, int y, int z) {
    return (x % 3 == 0 && y % 3 != 0 && z % 3 != 0) || (x % 3 != 0 && y % 3 == 0 && z % 3 != 0) || (x % 3 != 0 && y % 3 != 0 && z % 3 == 0);
  }

  public static boolean taskE(int x, int y, int z) {
    return x > 100 || y > 100 || z > 100;
  }
}