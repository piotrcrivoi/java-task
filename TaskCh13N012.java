﻿/**
 * Известна информация о 20 сотрудниках фирмы: фамилия, имя, отчество,
 * адрес и дата поступления на работу (месяц, год). Напечатать фамилию,
 * имя, отчество и адрес сотрудников, которые на сегодняшний день проработали
 *  в фирме не менее трех лет. День месяца не учитывать (при совпадении
 * месяца поступления и месяца сегодняшнего дня считать, что прошел полный год).
 * @author Crivoi Piotr
 */

import java.util.ArrayList;
import java.util.List;

public class TaskCh13N012 {
  public static void main(String[] args) {

    List<Human> humans = new ArrayList<>();
    Search search = new Search();

    humans.add(new Human("Алексеев", "Антон", "Павлович", "Вавилова 25", 1, 2015));
    humans.add(new Human("Алехин", "Павел", "Филиппович", "Нахимовский проспект 12", 2, 2015));
    humans.add(new Human("Булкин", "Андрей", "Юрьевич", "Львова 9", 11, 2014));
    humans.add(new Human("Зотов", "Евгений", "Иванов", "Губкина 3", 10, 2017));
    humans.add(new Human("Иванов", "Василий", "Борисович", "Винокурова 67", 10, 2014));
    humans.add(new Human("Петров", "Иван", "Петрович", "Шверкина 78", 1, 2015));
    humans.add(new Human("Пирогов", "Александр", "Дмитриевич", "Первомайская 13", 1, 2017));
    humans.add(new Human("Сафронов", "Дмитрий", "Григорьевич", "Измайловский проспект 6", 1, 2010));
    humans.add(new Human("Сидоров", "Николай", "Максимов", "Сиреневый бульвар 28", 1, 2014));
    humans.add(new Human("Хлебов", "Григорий", "Алексеевич", "Моховая 1", 1, 2015));
    humans.add(new Human("Матвеев", "Никита", "Станиславович", "Никольская 7", 1, 2013));
    humans.add(new Human("Макаренко", "Максим", "Сергеевич", "Гашека 20", 1, 2014));
    humans.add(new Human("Новоселов", "Алексей", "Владиславович", "Ильинка", 1, 2015));
    humans.add(new Human("Флейшер", "Пётр", "Станиславович", "Манежная 4", 1, 2012));
    humans.add(new Human("Гуров", "Борис", "Иванович", "Тихвинская 45", 1, 2011));
    humans.add(new Human("Антонов", "Илья", "Павлович", "Измайловский проспект 20", 1, 2013));
    humans.add(new Human("Ростовский", "Станислав", "Владимирович", "", 12, 2013));
    humans.add(new Human("Волохов", "Сергей", "Борисович", "Вавилова 6", 10, 2014));
    humans.add(new Human("Колосов", "Владимир", "Петрович", "Винокурова 20", 5, 2017));
    humans.add(new Human("Юсупов", "Владислав", "Андреевич", "Каретный ряд 5", 1, 2009));

    System.out.println("Сотрудники проработавшие 3 и более лет");
    search.searchByYear(2017, 11, humans);
  }
}


class Human {
  private String firstName;
  private String lastName;
  private String middleName;
  private String address;
  private int month;
  private int year;

  public Human (String lastName, String firstName, String middleName, String address, int month, int year) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.middleName = middleName;
    this.address = address;
    this.month = month;
    this.year = year;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public String getAddress() {
    return address;
  }

  public int getMonth() {
    return month;
  }

  public int getYear() {
    return  year;
  }

   public String toString() {
    return "ФИО сотрудника: " + getLastName() + " " + getFirstName() + " " + getMiddleName() + ". Проживает по адресу " + getAddress() + ". Начал работать в компании c xx/" + getMonth() + "/" + getYear();
  }

  public void getWorkerInfo() {

  }
}

class Search {

  public void searchByYear(int thisYear, int thisMonth, List<Human> humansList) {
    int time;
    for (int i = 0; i < humansList.size(); i++) {
      if (humansList.get(i).getMonth() < thisMonth) {
        time = thisYear - humansList.get(i).getYear() - 1;
      }
      else {
        time = thisYear - humansList.get(i).getYear();
      }
      if (time >= 3) {
        System.out.println(humansList.get(i));
      }
    }
  }
}
