﻿/**
 * Написать рекурсивную процедуру перевода натурального числа из десятичной
 * системы счисления в N-ричную. Значение N в основной программе вводится
 *  с клавиатуры (N >= 2 && N <= 16);
 * 
 * @author Crivoi Piotr
 */

import java.util.Scanner;

 class TaskCh10N055 {

  public static void main(String[] args) {


    Scanner in = new Scanner(System.in);
    Progression t = new Progression();
    System.out.print("Введите число ");
    int n = in.nextInt();
    System.out.print("Введите систему ");
    int sys = in.nextInt();

    t.prog(n, sys);

  }
}
class Progression {
  void prog(int n, int s) {
    if (n > 0) {
      tran(n / s, s);
      if ( s < 16) {

      System.out.print(n % s );
    } else if (s == 16) {
      System.out.printf("%X", n % s);
    }
    }
  }
}


