/**
 * �������� ����������� �������:
 * �) ���������� ����� ���� ������������ �����;
 * �) ���������� ���������� ���� ������������ �����.
 * @author Crivo Piotr
 */


import java.util.Scanner;

class TaskCh10N043 {
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    Calculation objCalculation = new Calculation();

    System.out.print("������� ����� = ");
    int n = in.nextInt();

    System.out.println("����� ���� = " + objCalculation.sumOfNumbers(n));
    System.out.println("���������� ���� = " + objCalculation.countOfNumbers(n));
  }
}



class Calculation {
  int sumOfNumbers(int n) {

    int result = 0;

    if (n == 0)
      return 0;
    else
      result = result + n % 10;
    result = result + sumOfNumbers(n / 10);
    return result;
  }

  int countOfNumbers(int n) {

    int count = 0;

    if (n == 0)
      return 0;
    else
      count = 1 + countOfNumbers(n/10);
    return count;
  }
}