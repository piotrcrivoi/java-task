/**
 * ������� �� ������� ��� ������������� ��������, ������� �� ������ ���������, �. �.
 * � ������� ������ �������� ������ ��������� ��������.
 *
 * @author Crivoi Piotr
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class TaskCh11N158 {
  public void main() {

    Scanner in = new Scanner(System.in);

    ArrayList<Integer> numbers = new ArrayList<>();

    int count;
    int countNumbers = 0;

    System.out.print("����� ���������� ��������� � ������� - ");
    int n = in.nextInt();

    for (int i = 0; i < n; i++) {
      System.out.print("����� " + i + " ������� ������� - ");
      numbers.add(in.nextInt());
    }

    for (int i = numbers.size() - 1; i >= 0; i--) {
      count = Collections.frequency(numbers, numbers.get(i));
      if (count > 1) {
        numbers.remove(numbers.get(i));
        countNumbers++;
      }
    }

    for (int i = 0; i <= countNumbers; i++) numbers.add(0);

    for (int i = 0; i < numbers.size(); i++) {
      System.out.print(numbers.get(i) + " ");
    }
  }
}
