/**
 * ���� ��� ����� ����� a � b. ���� a ������� �� b ��� b ������� �� a, ��
 * ������� 1, ����� � ����� ������ �����.
 * �������: �������� ��������� � ��������� ����� �� ������������.
 *@author Crivoi Piotr
 */

import java.util.Scanner;


class TaskCh02N043 {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
      int c;

    System.out.println("������� a: ");
      int a = in.nextInt();
    System.out.println("������� b:");
      int b = in.nextInt();
    c = (a % b) * (b % a);
    System.out.println("����� = " +  (c + 1));
  }
}