﻿/**
 * Дан двумерный массив.
 * а) Удалить из него k-ю строку.
 * б) Удалить из него s-й столбец
 *
 * @author Crivoi Piotr
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskCh12N234 {
  public static void main(String[] args) {
    int k = 1;
    Scanner in = new Scanner(System.in);
    List<List<Integer>> list = new ArrayList<>();
    System.out.print("Ввведите размерность массива: ");
    int n = in.nextInt();

    for (int i = 0; i < n; i++) {
      list.add(new ArrayList<>());
      for (int j = 0; j < n; j++) {

        list.get(i).add(k++);
      }
    }
    for (int i = 0; i < list.size(); i++) {
      for (int j = 0; j < list.size(); j++) {
        System.out.printf("%3d", list.get(i).get(j));
      }
      System.out.println();
    }

    System.out.print("Введите строку, которую хотите удалить: ");
    int row = in.nextInt();

    list.remove(row - 1);
    list.add(new ArrayList<>());

    for (int i = 0; i < list.size() - 1; i++) {
      for (int j = 0; j < list.size(); j++) {
        System.out.printf("%3d", list.get(i).get(j));
      }
      System.out.println();
    }

    System.out.println("Добавляем строку,заполненную нулями");
//
    for (int j = 0; j < list.size(); j++) {
      list.get(list.size() - 1).add(0);
    }

    for (int i = 0; i < list.size(); i++) {
      for (int j = 0; j < list.size(); j++) {
        System.out.printf("%3d", list.get(i).get(j));
      }
      System.out.println();
    }
    System.out.print("Введите столбец, который хотите удалить: ");
    int column = in.nextInt();

    for (int i = 0; i < list.size(); i++) {
      list.get(i).remove(column - 1);

    }
    for (int i = 0; i < list.size(); i++) {
      for (int j = 0; j < list.size() - 1; j++) {
        System.out.printf("%3d", list.get(i).get(j));
      }
      System.out.println();
    }
    System.out.println("Добавляем столбец,заполненный нулями");
    for (int i = 0; i < list.size(); i++) {
      list.get(i).add(0);
    }

    for (int i = 0; i < list.size(); i++) {
      for (int j = 0; j < list.size(); j++) {
        System.out.printf("%3d", list.get(i).get(j));
      }
      System.out.println();
    }


  }
}
