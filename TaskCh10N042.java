/**
 * � ��������� ������ ���������������� (��������, � �������) �� 
 * ������������� �������� ���������� � �������. �������� ����������� �������
 * ��� ������� ������� n ������������� ����� a (n � ����������� �����).
 * @author Crivoi Piotr
 */

import java.util.Scanner;

class TaskCh10N042 {

  public static void main(String args[]) {

    Scanner in = new Scanner(System.in);

    Power p = new Power();

    System.out.print("������� �����: ");
      int a = in.nextInt();
    System.out.print("������� �������: ");
      int n = in.nextInt();
    System.out.print("  ����� � ������� " + n + ", ����� = " + p.pow(a, n));
  }
}
class Power {
  int pow(int a, int n) {
    if (n == 0) return 1;
    else {
      return a*pow(a,n-1);
    }
  }
}