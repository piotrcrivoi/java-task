/**
 * � ������� 12 �������. �������� ���������� ������� (� ������� �������)
 * � ������� (� ��2) ������� ������. ���������� ������� ��������� ���������
 * �� ������� � �����.
 *
 * @author Crivoi Piotr
 */

public class Main {

  public static void main(String[] args) {

    TaskCh05N064 javatask = new TaskCh05N064();

    int[][] population = {{1000, 150},
            {3000, 100},
            {9600, 400},
            {2450, 960},
            {8600, 270},
            {6000, 860},
            {1200, 170},
            {3400, 170},
            {2900, 126},
            {5000, 700},
            {1500, 300},
            {1600, 570}};

    javatask.main(population);

  }
}

class TaskCh05N064 {
  public void main(int[][] population) {

    int people = 0;
    int square = 0;

    for (int i = 0; i < population.length; i++) {
      people += population[i][0];
      square += population[i][1];
    }

    System.out.print("������� ��������� ��������� - " + people/square);
  }
}