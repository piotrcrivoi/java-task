﻿/**
 * Написать рекурсивную функцию для вычисления k-го члена последовательности Фибоначчи.
 * Последовательность Фибоначчи.
 * @author Crivoi Piotr
 */

import java.util.Scanner;

class TaskCh10N047 {

  public static void main(String[] args) {


    Scanner in = new Scanner(System.in);
    Fibonacci f = new Fibonacci();

    System.out.print("Введите индекс члена: ");
    int k = in.nextInt();
    
    System.out.println(k + "-ый элемент = " + f.fibElem(k));
  }
}

class Fibonacci {
  int fibElem(int k) {
    if (k == 0) {
      return 0;
    }else if (k == 1) {
      return  1;
    } else {
      return fibElem(k - 1) + fibElem(k -2);
    }
  }
}


