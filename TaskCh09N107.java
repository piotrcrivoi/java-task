/**
 * ���� �����. �������� ������� ������ �� ���� � � ��������� �� ���� �.
 * ������ ����������� ����, ��� ����� ���� � ����� ����� �� ����.
 * @author Crivoi Piotr
 */

import java.util.Scanner;

class TaskCh09N107 {
  public static void main(String args[]) {
    int l, i, sumA, sumO;
    int a = 0;
    int b = 0;
    Scanner in = new Scanner(System.in);

    System.out.print("������� �����: ");
      StringBuilder stringBuilder = new StringBuilder(in.nextLine());

      l = stringBuilder.length();
      sumA = 0;
      sumO = 0;
    for (i = 0; i < l; i++) {
      if (stringBuilder.charAt(i) == 'a') sumA = sumA + 1;
      if (stringBuilder.charAt(i) == 'o') sumO = sumO + 1;
    }
    if(sumA >= 1 && sumO >= 1) {

      for (i = 0; i < l; i++) {
        if (stringBuilder.charAt(i) == 'a') {
          a = i;
          break;
        }
      }
      stringBuilder.setCharAt(a, 'o');
      for (i = 0; i < l; i++) {
        if (stringBuilder.charAt(i) == 'o') {
          b = i;
          break;
        }
      }
      stringBuilder.setCharAt(b, 'a');
      System.out.print(stringBuilder);
    } else {
      System.out.print("� ������ ����� � = " + sumA + "��, o = " + sumO + "��.");
    }
  }
}


