/**
 * � ����������� ����� x ���������� ��� ������ �����. ����� � �������������
 * ��� ���� ����������� ����� ������ ��������� ������ ����� ����� x, �� 
 * ���������� ����� n. �� ��������� n ����� ����� x. (�������� n �������� � ����������.
 * �������:�������� n �������� � ����������, (n >= 100 && n <= 999).
 * @author Crivoi Piotr
 */


import java.util.Scanner;


public class TaskCh02N031 {

  public static void main(String[] args) {
    int  x;

    Scanner sc = new Scanner(System.in);
    System.out.printf("������� ����� = \n");
    int n = sc.nextInt();
    x = 0;
    if (n >= 100 && n <= 999) {
      x = n / 10;
      x = (x / 10) * 100 + (x % 10);
      x = x + (n % 10)*10;
      System.out.println("���������� ����� - " + x);
    }
    else System.out.println("�������� ����� - " + n + ",��������� ��� ���������.");
  }
}